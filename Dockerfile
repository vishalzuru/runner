FROM nginx:latest
COPY index.html /usr/local/nginx/html
EXPOSE 80
ENTRYPOINT ["nginx","-g","daemon off;"]